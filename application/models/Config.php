<?php

class Application_Model_Config
{
	public function getDadosUnidade($unidade_id)
	{
		$_db_config = new Application_Model_DbTable_Config();
	
		$unidade = $_db_config->fetchRow('id = '.$unidade_id);
	
		$dados_unidade = array('id' => $unidade['id'],
				'nome' => $unidade['nome'],
				'img_url' => $unidade['img_url'],
				'img_descricao' => $unidade['img_descricao']);
	
		return $dados_unidade;
	}
	
	public function getDadosUnidadePorLocal($unidade)
	{
		$_db_config = new Application_Model_DbTable_Config();
	
		$unidade = $_db_config->fetchRow('local = "'.$unidade.'"');
	
		$dados_unidade = array('id' => $unidade['id'],
				'nome' => $unidade['nome'],
				'img_url' => $unidade['img_url'],
				'img_descricao' => $unidade['img_descricao']);
	
		return $dados_unidade;
	}
}

