<?php

class Application_Model_DbTable_Secao extends Zend_Db_Table_Abstract
{

    protected $_name = 'inst_secao';
    protected $_primary = 'id';
    
    protected $_referenceMap = array(
    		'Secao' => array(
    				'columns'=>array('idSuperior'),
    				'refTableClass'=>'Application_Model_DbTable_Secao',
    				'refColumns'=>array('id')
    		),
    );
    
    protected $_dependentTables = array(
    		'Application_Model_DbTable_Secao');

    
    
}

