<?php

class Application_Model_DbTable_Publicacao extends Zend_Db_Table_Abstract
{

    protected $_name = 'publicacoes';

    protected $_primary = 'id';
       
    protected $_referenceMap = array(
    		'Categoria' => array(
    				'columns'=>array('categoria'),
    				'refTableClass'=>'Application_Model_DbTable_CategoriaPublicacao',
    				'refColumns'=>array('id')
    		),
    );

}

