<?php

class Application_Model_Parceiros
{
	public function getParceirosPorUnidade($unidade_id)
	{
		$_db_parceiros = new Application_Model_DbTable_Parceiros();
	
		$parceiros = $_db_parceiros->fetchAll('id_unidade = '.$unidade_id);
	
		$todos_parceiros = array();
	
		foreach($parceiros as $dados_parceiros)
		{
			$todos_parceiros[] = array('href' => $dados_parceiros['img_href'],
					'alt' => $dados_parceiros['img_titulo'],
					'src' => $dados_parceiros['img_url'],
					'nome_sistema' => $dados_parceiros['nome_parceiro'],
					'descricao' => $dados_parceiros['descricao']);
		}
	
		return $todos_parceiros;
	}
}