<?php

class Application_Model_Secao {
	private $id;
	private $idSuperior;
	private $descricao;
	private $link;
	private $texto;
	private $nivel;
	private $observacao;
	private $localidade;
	private $menu;
	private $prioridade;
	private $situacao;
	private $externo;
	private $linkExterno;
	private $target;
	private $localizacao;
	private $secoesInferiores;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getIdSuperior() {
		return $this->idSuperior;
	}

	public function setIdSuperior($idSuperior) {
		$this->idSuperior = $idSuperior;
	}

	public function getDescricao() {
		return $this->descricao;
	}

	public function setDescricao($descricao) {
		$this->descricao = $descricao;
	}

	public function getLink() {
		return $this->link;
	}

	public function setLink($link) {
		$this->link = $link;
	}

	public function getTexto() {
		return $this->texto;
	}

	public function setTexto($texto) {
		$this->texto = $texto;
	}

	public function getNivel() {
		return $this->nivel;
	}

	public function setNivel($nivel) {
		$this->nivel = $nivel;
	}

	public function getObservacao() {
		return $this->observacao;
	}

	public function setObservacao($observacao) {
		$this->observacao = $observacao;
	}

	public function getLocalidade() {
		return $this->localidade;
	}

	public function setLocalidade($localidade) {
		$this->localidade = $localidade;
	}

	public function getMenu() {
		return $this->menu;
	}

	public function setMenu($menu) {
		$this->menu = $menu;
	}

	public function getPrioridade() {
		return $this->prioridade;
	}

	public function setPrioridade($prioridade) {
		$this->prioridade = $prioridade;
	}

	public function getSituacao() {
		return $this->situacao;
	}

	public function setSituacao($situacao) {
		$this->situacao = $situacao;
	}

	public function getExterno() {
		return $this->externo;
	}

	public function setExterno($externo) {
		$this->externo = $externo;
	}

	public function getLinkExterno() {
		return $this->linkExterno;
	}

	public function setLinkExterno($linkExterno) {
		$this->linkExterno = $linkExterno;
	}

	public function getTarget() {
		return $this->target;
	}

	public function setTarget($target) {
		$this->target = $target;
	}

	public function getLocalizacao() {
		return $this->localizacao;
	}

	public function setLocalizacao($localizacao) {
		$this->localizacao = $localizacao;
	}

	public function getSecoesInferiores() {
		return $this->secoesInferiores;
	}

	public function setSecoesInferiores($secoesInferiores) {
		$this->secoesInferiores = $secoesInferiores;
	}
	
	
}

