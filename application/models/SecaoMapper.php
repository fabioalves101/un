<?php

class Application_Model_SecaoMapper extends Zend_Db_Table_Abstract
{
	private $db;
	public function __construct() {
		$this->db = new Application_Model_DbTable_Secao();
	}
	
	
	public function getSecoes($local, $nivel, $situacao) {		
		$registros = $this->db->fetchAll(
				$this->db->select()
				->where('localidade = ?', $local)
				->where('idSuperior = ?', $nivel)
				->where('situacao = ?', $situacao)
				->order(array('prioridade asc'))
		);
		
	
		$secoes = null;
			if($registros->count() != 0) {
			foreach ($registros as $sec) {
				$secao = new Application_Model_Secao();
				$secao->setId($sec->id);
				$secao->setIdSuperior($sec->idSuperior);
				$secao->setDescricao($sec->descricao);
				$secao->setLink($sec->link);
				$secao->setTexto($sec->texto);
				$secao->setNivel($sec->nivel);
				$secao->setObservacao($sec->observacao);
				$secao->setLocalidade($sec->localidade);
				$secao->setMenu($sec->menu);
				$secao->setPrioridade($sec->prioridade);
				$secao->setSituacao($sec->situacao);
				$secao->setExterno($sec->externo);
				$secao->setLinkExterno($sec->linkExterno);
				$secao->setTarget($sec->target);
				$secao->setLocalizacao($sec->localizacao);
				
				$secao->setSecoesInferiores(						
						$this->getSecoes($local, $secao->getId(), $situacao)
				);
					
				$secoes[] = $secao;
			}
		}
	
		return $secoes;
	}
	
	public function getSecao($id) {
		$db = new Application_Model_DbTable_Secao();
		
		$registro = $db->fetchRow("id = ".$id);
		
		if($registro != null) {		
			$secao = new Application_Model_Secao();
			$secao->setId($registro->id);
			$secao->setIdSuperior($registro->idSuperior);
			$secao->setDescricao($registro->descricao);
			$secao->setLink($registro->link);
			$secao->setTexto($registro->texto);
			$secao->setNivel($registro->nivel);
			$secao->setObservacao($registro->observacao);
			$secao->setLocalidade($registro->localidade);
			$secao->setMenu($registro->menu);
			$secao->setPrioridade($registro->prioridade);
			$secao->setSituacao($registro->situacao);
			$secao->setExterno($registro->externo);
			$secao->setLinkExterno($registro->linkExterno);
			$secao->setTarget($registro->target);
			$secao->setLocalizacao($registro->localizacao);
			
			$secao->setSecoesInferiores(
						$this->getSecoes($registro->localidade, $registro->id, 'a')
					);
		} else {
			$secao = null;
		}
		return $secao;
	}

}

