<?php

class Application_Model_Sistemas
{
	public function getSistemasPorUnidade($unidade_id)
	{
		$_db_sistemas = new Application_Model_DbTable_Sistemas();
		
		$sistemas = $_db_sistemas->fetchAll('id_unidade = '.$unidade_id);
		
		$todos_sistemas = array();
		
		foreach($sistemas as $dados_sistema)
		{
			$todos_sistemas[] = array('href' => $dados_sistema['img_href'],
										'title' => $dados_sistema['img_titulo'],
										'src' => $dados_sistema['img_url'],
										'nome_sistema' => $dados_sistema['nome_sistema'],
										'descricao' => $dados_sistema['descricao']);		
		}
		
		return $todos_sistemas;		
	}

}

