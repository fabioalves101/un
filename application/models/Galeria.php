<?php

class Application_Model_Galeria
{
	public function getImagensSecao($secaoUID)
	{
		$_db_imagem = new Application_Model_DbTable_Imagem();
		$_db_secao = new Application_Model_DbTable_Secao();
		$_db_galeria = new Application_Model_DbTable_Galeria();
		
		$imagens_galeria = $_db_galeria->fetchAll('secaoUID = '.$secaoUID);
		
		$galeria = array();
		foreach($imagens_galeria as $dados_galeria)
		{
			$dados_imagem = $_db_imagem->fetchRow('imagemUID ='.$dados_galeria['imagemUID']);
			$galeria[$secaoUID][] = array('imagemUID' => $dados_galeria['imagemUID'],
								'nomeImagem' => $dados_imagem['nomeImagem'],
								'arquivo' => $dados_imagem['arquivo']);	
		}
		
		return $galeria;	
	}
}

