<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initWithRoutes() {
		$front = Zend_Controller_Front::getInstance();
		$router = $front->getRouter();
		
		
		$router->addRoute('secaoroute',
				new Zend_Controller_Router_Route('/secao/:id/:unidade',
						array('controller' => 'secao', 'action' => 'index'))
		);
		
		$router->addRoute('indexroute',
				new Zend_Controller_Router_Route(':unidade',
						array('controller' => 'site', 'action' => 'index'))
		);
		
		$router->addRoute('noticiaroute',
				new Zend_Controller_Router_Route('/noticia/:id/:unidade',
						array('controller' => 'noticia', 'action' => 'index'))
		);
		/*
		$router->addRoute('todasnoticiaroute',
				new Zend_Controller_Router_Route('/noticias/:unidade/',
						array('controller' => 'noticia', 'action' => 'list'))
		);
		
		
		$router->addRoute('todasnoticiapagroute',
				new Zend_Controller_Router_Route('/noticias/:unidade/:pagina',
						array('controller' => 'noticia', 'action' => 'list'))
		);
		*/
		$router->addRoute('contatoroute',
				new Zend_Controller_Router_Route('/contato/:unidade',
						array('controller' => 'contato', 'action' => 'index'))
		);	
		
		$router->addRoute('publicacaoroute',
				new Zend_Controller_Router_Route('/publicacao/:unidade',
						array('controller' => 'publicacao', 'action' => 'index'))
		);
		
		$router->addRoute('publicacaopagroute',
				new Zend_Controller_Router_Route('/publicacao/:unidade/:pagina',
						array('controller' => 'publicacao', 'action' => 'index'))
		);
		
		$router->addRoute('todasnoticiaroute',
				new Zend_Controller_Router_Route('/sistemas/:unidade/',
						array('controller' => 'sistemas', 'action' => 'list'))
		);
		
	}
	
	protected function _initMail() {
		$smtpServer = '200.17.60.1';
		$username = 'contato';
		$password = 'pd578q25v';
		 
		$config = array('ssl' => 'tls',
				'auth' => 'login',
				'username' => $username,
				'password' => $password);
		
		$transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);
		Zend_Mail::setDefaultTransport($transport);
	}
	
	protected function _initAutoloader()
	{
		new Zend_Application_Module_Autoloader(array(
				'namespace' => 'Application',
				'basePath'  => APPLICATION_PATH . '/modules/default',
		));
	}
	
	protected function _initAutoload()
	{
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->setFallbackAutoloader(true);				
	}
	
	
}

