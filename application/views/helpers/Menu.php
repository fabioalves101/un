<?php
class Zend_View_Helper_Menu extends Zend_View_Helper_Abstract
{	
	public function menu($local)
	{
		Zend_Loader::loadClass('SecaoController', 'application/controllers');
		
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$response = Zend_Controller_Front::getInstance()->getResponse(); 
		
		$secaoController = new SecaoController($request, $response);
		return $secaoController->GetSecoesRaiz(Zend_Registry::get('local'));
	}
	
}