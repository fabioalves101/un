<?php
class Zend_View_Helper_Util extends Zend_View_Helper_Abstract
{	

	public function limitString($string, $tamanho, $encode = 'UTF-8') {
		if(strlen($string) > $tamanho )
			$string = mb_substr($string, 0, $tamanho - 3, $encode) . '...';
		else
			$string = mb_substr($string, 0, $tamanho, $encode);
	
		return $string;
	}
}