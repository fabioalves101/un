<?php
class Zend_View_Helper_Campus extends Zend_View_Helper_Abstract
{	
	public function campus($id)
	{
		Zend_Loader::loadClass('CampusController', 'application/controllers');
		
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$response = Zend_Controller_Front::getInstance()->getResponse(); 
		
		$controller = new CampusController($request, $response);
		
		return $controller->getCampus($id);
	}
	
}