<?php
class Zend_View_Helper_CssHelper extends Zend_View_Helper_Abstract
{
	function cssHelper($cssfilename = null) {
		if($cssfilename != null) {
			
			$file_uri = $this->view->baseUrl().'/public/estilo/' . $cssfilename . '.css';
			 
			
				$this->view->headLink()->appendStylesheet($file_uri);
			
			 
			return $this->view->headLink();
		} else {
			return "";
		}
		 
	}
}