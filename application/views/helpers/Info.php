<?php
class Zend_View_Helper_Info extends Zend_View_Helper_Abstract
{	
	public function info($local)
	{
		Zend_Loader::loadClass('SiteController', 'application/controllers');
		
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$response = Zend_Controller_Front::getInstance()->getResponse(); 
		
		$controller = new SiteController($request, $response);
		
		return $controller->getSite($local);
	}
	
}