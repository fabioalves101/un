<?php

class PublicacaoController extends Zend_Controller_Action
{	
    public function init()
    {
        $this->_db = new Application_Model_DbTable_Publicacao();
    }

    public function indexAction()
    {    	    	
    	$unidade = $this->_getParam('unidade');
    	$pagina = $this->_getParam('pagina');
    	Zend_Registry::set('local', $unidade);    	
    	
    	$this->view->categorias = $this->getCategorias($unidade);
    	
    	$this->view->idcategoria = ($this->_getParam('categoria') != "") ? $this->_getParam('categoria') : "";
    	$this->view->busca = ($this->_getParam('busca') != "") ? $this->_getParam('busca') : "";
    	$this->view->filtro = ($this->_getParam('filtro') != "") ? $this->_getParam('filtro') : "";

    	$querystring = "?busca=".$this->view->busca."&filtro=".$this->view->filtro."&categoria=".$this->view->idcategoria;
    	Zend_Registry::set('querystring', $querystring);
    	
    	$publicacoes = $this->getPublicacoes($this->view->busca, $this->view->idcategoria, $this->view->filtro, $unidade);
    		
    	$paginator = Zend_Paginator::factory($publicacoes);
    	$paginator->setItemCountPerPage(5);
   		$paginator->setPageRange(7);
   		$paginator->setCurrentPageNumber($pagina);
   		$this->view->pagina = $pagina;
   		$this->view->publicacoes = $paginator;
    	
    	$this->view->cssfilename = "publicacoes";
    }
    
    public function getCategorias($unidade) {
    	Zend_Loader::loadClass('SiteController', 'application/controllers');
    	$sitecontroller = new SiteController($this->_request, $this->_response);
    	 
    	$site = $sitecontroller->getSite($unidade);
    	 
    	$dbcategoria = new Application_Model_DbTable_CategoriaPublicacao();
    	return $dbcategoria->fetchAll(
    			$dbcategoria->select()
    			->where("idsite = ?", $site->id)
    	);
    }
    
    public function getPublicacoes($busca, $categoria, $filtro, $unidade) {
    	Zend_Loader::loadClass('SiteController', 'application/controllers');
    	$sitecontroller = new SiteController($this->_request, $this->_response);
    	
    	$site = $sitecontroller->getSite($unidade);
    	
    	if($busca != "") {
	    	if($categoria != "") {
		    	return 
		    	$this->_db->fetchAll(
		    				$this->_db->select()
		    					->where("categoria = ?", $categoria)
		    					->where($filtro." like ?", '%'.$busca.'%')	    			
		    					->where("local = ?", $site->id)
		    					->order('id DESC')
		    			);
	    	} else {
		    	return
		    	$this->_db->fetchAll(
		    			$this->_db->select()    			
		    			->where($filtro." like ?", '%'.$busca.'%')
		    			->where("local = ?", $site->id)
		    			->order('id DESC')
		    	);
	    	}
    	}
    	else {
    		if($categoria != "") {
    			return
    			$this->_db->fetchAll(
    					$this->_db->select()
    					->where("categoria = ?", $categoria)
    					->where("local = ?", $site->id)
    					->order('id DESC')
    					);
    		} else {
	    		return
	    		$this->_db->fetchAll(
	    				$this->_db->select()
	    				->where("local = ?", $site->id)
	    				->order('id DESC')
	    		);
    		}    		
    	}
    }
}

