<?php

class ContatoController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_DbTable_Contato();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    public function indexAction()
    {
		$unidade = $this->_getParam('unidade');
		
		$enviado = $this->_getParam('enviado');
		
		
		Zend_Registry::set('local', $unidade);

		if($enviado != null) {
			if($this->_flashMessenger->hasMessages()) {
				foreach ($this->_flashMessenger->getMessages() as $m) {
					$this->view->sucesso .= $m;
				}
			}
		} else {
		
			if($this->_flashMessenger->hasMessages()) {
				foreach ($this->_flashMessenger->getMessages() as $m) {
					$this->view->mensagem .= $m;
				}
			}
		}
		
		Zend_Loader::loadClass('SiteController', 'application/controllers');
		$site = new SiteController($this->_request, $this->_response);
		$this->view->unidade = $site->getSite($unidade);
		$this->view->destinatarios = $this->getDestinatarios($unidade);
		$this->view->cssfilename = "contato";
    }

    public function enviarAction()
    {
    	$unidade = $this->_getParam('unidade');
    	Zend_Registry::set('local', $unidade);

    	require_once APPLICATION_PATH . '/../public/securimage/securimage.php';
    	
    	$securimage = new Securimage();
    	if ($securimage->check($_POST['captcha_code']) == false) {
    		$this->_flashMessenger->addMessage("Os caracteres informados não estão corretos.");
    		$this->_redirector->gotoSimple('index', 'contato', null, array('unidade' => $unidade));		 
    	} else {
    	
	    	$destinatario = $this->_request->getPost('destinatario');
	    	$nome = $this->_request->getPost('nome');
	    	$email = $this->_request->getPost('email');
	    	$mensagem = $this->_request->getPost('mensagem');
	    	
	    	
	    	$mail = new Zend_Mail('utf-8');
	    	$mail->setBodyHtml($mensagem);
	    	$mail->setFrom($email, $nome);
	    	$mail->addTo($destinatario, 'Contato Site '.$unidade);
	    	$mail->send();
	    	
	    	$this->_flashMessenger->addMessage("Mensagem enviada com sucesso");
	    	$this->_redirector->gotoSimple('index', 'contato', null, array('unidade' => $unidade, "enviado" => true));
    	}
    }
    
    public function getDestinatarios($unidade) {
    	return $this->_db->fetchAll(
    			$this->_db->select()
    			->where("instituto = '".$unidade."'")
    	);
    }


}



