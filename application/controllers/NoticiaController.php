<?php

class NoticiaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_DbTable_Noticia();
    }

    public function indexAction()
    {
    	$unidade = $this->_getParam('unidade');
    	Zend_Registry::set('local', $unidade);
    	 
    	$id = $this->_getParam('id');
    	 
    	$this->view->noticia = $this->_db->fetchRow("id = ".$id);
    	
    	
    }


    public function getNoticias($local, $publicada)
    {
    	$status = 'publicada';
    	if(!$publicada) {
    		$status = 'excluida';
    	}    	
    	
    	$noticias = $this->_db->fetchAll(
    				$this->_db->select()
    					->where("local = ?", $local)
    					->where("status = ?", $status)
    					->order('id DESC')
    			);
    	
    	return $noticias;
    }
    
    public function getNoticiasByDestaque($local, $publicada, $destaque, $limit, $offset)
    {
    	$status = 'publicada';
    	if(!$publicada) {
    		$status = 'excluida';
    	}
    	 
    	$noticias = $this->_db->fetchAll(
    			$this->_db->select()
    			->where("local = ?", $local)
    			->where("status = ?", $status)
    			->where("destaque = ?", $destaque)
    			->limit($limit, $offset)
    			->order('id DESC')
    	);
    	 
    	return $noticias;
    }
    

    public function listAction()
    {
     	$unidade = $this->_getParam('unidade');
     	$pagina = $this->_getParam('pagina', 1);
    	Zend_Registry::set('local', $unidade);
    	
    	$noticias = $this->getNoticias($unidade, true);
    	    	
    	$paginator = Zend_Paginator::factory($noticias);
    	// Seta a quantidade de registros por página
    	$paginator->setItemCountPerPage(10);
    	// numero de paginas que serão exibidas
    	$paginator->setPageRange(7);
    	// Seta a página atual
    	$paginator->setCurrentPageNumber($pagina);
    	// Passa o paginator para a view
    	$this->view->pagina = $pagina;
    	
    	$this->view->noticias = $paginator;
    	
    	$this->view->cssfilename = "noticia";
    }


}



