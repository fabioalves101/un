<?php

class SecaoController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_SecaoMapper();
    }

    public function indexAction()
    {        	
    	$unidade = $this->_getParam('unidade');
    	Zend_Registry::set('local', $unidade);
    	
    	$id = $this->_getParam('id');
    	
    	$secao = $this->_db->getSecao($id);
    	$this->view->secao = $secao;
    	    	
    	$this->view->secoesEsquerda = new Application_Model_Secao();
    	$this->view->secoesDireita = new Application_Model_Secao();
    	$model_galeria = new Application_Model_Galeria();
    	
    	$qtdeSecoesInferiores = count($secao->getSecoesInferiores());
    	
    	if($qtdeSecoesInferiores > 0) {
    		$this->view->secoesEsquerda = $this->getEstruturaSecoesEsquerda($qtdeSecoesInferiores, $secao->getSecoesInferiores());
    		$this->view->secoesDireita = $this->getEstruturaSecoesDireita($qtdeSecoesInferiores, $secao->getSecoesInferiores());
    		$this->view->cssfilename = 'menucircular';
    	} else {
    		$this->view->cssfilename = 'nomenu';
    	}   
    	 		
    	$galeria_secao = $model_galeria->getImagensSecao($id);
    	if(count($galeria_secao)){
    		$this->view->galeria_secao = $galeria_secao[$id]; 
    	}  	
    	
    }
    
    
    public function getEstruturaSecoesEsquerda($qtdeSecoesInferiores, $secoesInferiores) {
    	   	if($qtdeSecoesInferiores % 2 == 0) {
    	   		$cont1 = $qtdeSecoesInferiores / 2;
    			$arr =  $secoesInferiores;
    			 
    			$l1 = null;
    			for($i = 0; $i < $cont1; $i++) {
    				$l1[] = $arr[$i];
    			}
    			 
    			
    		} else {
    			
    			if($qtdeSecoesInferiores != 1) {    				
    				$cont1 = round($qtdeSecoesInferiores / 2);    				
    				$arr =  $secoesInferiores;
    				 
    				$l1 = null;
    	
    				for($i = 0; $i < $cont1; $i++) {
    					$l1[] = $arr[$i];
    				}    				 
    				
    			} else {
    				$l1 = null;
    				$arr =  $secoesInferiores;
    				for($i = 0; $i < $qtdeSecoesInferiores; $i++) {
    					$l1[] = $arr[$i];
    				}
    				
    			}
    		}   	
    	
    	return $l1;
    }
    
    public function getEstruturaSecoesDireita($qtdeSecoesInferiores, $secoesInferiores) {
    	$arr =  $secoesInferiores;
    	if($qtdeSecoesInferiores % 2 == 0) {
    		$cont1 = $qtdeSecoesInferiores / 2;
    		
    		
    		$l2 = null;
    		for($i = $cont1; $i < (2*$cont1); $i++) {
    			$l2[] = $arr[$i];
    		}
    	} else {
    		if($qtdeSecoesInferiores != 1) {
    			$cont1 = round($qtdeSecoesInferiores / 2);
    			
	    		$l2 = null;
	    		for($i = $cont1; $i < (2*$cont1-1); $i++) {
	    			$l2[] = $arr[$i];
	    		}
    		} else {
    			$l2 = new Application_Model_Secao();
    		}
    	}
    	
    	return $l2;
    }
    
    
    
    public function GetSecoesRaiz($local) {
    	return $this->_db->getSecoes($local, 0, 'a');
    }
    
    


}

