<?php

class SiteController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_DbTable_Site();
    }

    public function indexAction()
    {   	   	    	
    	$model_config = new Application_Model_Config();
    	$model_sistemas = new Application_Model_Sistemas();
    	$model_parceiros = new Application_Model_Parceiros();    	
    	
    	$unidade = $this->_request->getParam('unidade');
    	
    	if($unidade == null)
    		$unidade = 'sti';
    	
    	Zend_Registry::set('local', $unidade);
    	$site = $this->_db->fetchRow("local = '".$unidade."'");
		$this->view->site = $site;
		
		Zend_Registry::set('nomesite', $site->nome);
				
		Zend_Loader::loadClass('NoticiaController', 'application/controllers');	
		Zend_Loader::loadClass('SistemasController', 'application/controllers');
		$noticiaController = new NoticiaController($this->getRequest(), $this->getResponse());
		$sistemasController = new SistemasController($this->getRequest(), $this->getResponse());
		
		$this->view->noticias = $noticiaController->getNoticiasByDestaque($site->local, true, 0, 4, 0);
		$this->view->destaques = $noticiaController->getNoticiasByDestaque($site->local, true, 1, 4, 0);
		$this->view->sistemas = $model_sistemas->getSistemasPorUnidade($site->id);
		$this->view->parceiros = $model_parceiros->getParceirosPorUnidade($site->id);		
		$this->view->dados_config = $site;
	}
	
	public function getSite($unidade) {	
		$site = $this->_db->fetchRow("local = '".$unidade."'");
		return  $site;
	}
}