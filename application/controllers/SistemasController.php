<?php

class SistemasController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$unidade = $this->_getParam('unidade');
    	Zend_Registry::set('local', $unidade);
    	 
    }

    public function listAction()
    {
    	$model_sistemas = new Application_Model_Sistemas();
    	$model_config = new Application_Model_Config();
    	
    	$unidade = $this->_getParam('unidade');
    	Zend_Registry::set('local', $unidade);
    	
    	$unidade_id = $model_config->getDadosUnidadePorLocal($unidade);

    	$this->view->sistemas = $model_sistemas->getSistemasPorUnidade($unidade_id['id']);
    }

}



