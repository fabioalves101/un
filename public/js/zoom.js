$(document).ready(function(){
    var fonte = 17;
    $('#aumenta_fonte').click(function(){
        if (fonte < 19){
            fonte = fonte+1;
            $('body').css({
                'font-size' : fonte+'px'
            });
        }
    });
    $('#reduz_fonte').click(function(){
        if (fonte > 13){
            fonte = fonte-2;
            $('body').css({
                'font-size' : fonte+'px'
            });
            
        }
    });
    $('#volta_padrao').click(function(){
        $('body').css({
            'font-size' : '12px'
        });	
    });	
});