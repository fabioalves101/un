$(function() {
	$(".cursosDestaque").jCarouselLite({
		btnNext: ".next",
		btnPrev: ".prev",
		auto: 7000,
		speed: 1000,
		visible: 4,
		scroll: 4
	});
});    
